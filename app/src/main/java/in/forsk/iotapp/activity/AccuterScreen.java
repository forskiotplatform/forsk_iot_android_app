package in.forsk.iotapp.activity;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import in.forsk.iotapp.utils.AsyncApp42ServiceApi;
import in.forsk.iotapp.utils.AsyncApp42ServiceApi.App42StorageServiceListener;
import in.forsk.iotapp.IotApplication;
import in.forsk.iotapp.R;
import in.forsk.iotapp.utils.PreferencesManager;
import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.shephertz.app42.paas.sdk.android.App42Exception;
import com.shephertz.app42.paas.sdk.android.storage.Storage;

public class AccuterScreen extends Activity {

	ToggleButton tb1, tb2, tb3;
	SeekBar sb1, sb2, sb3;
	EditText et1, et2, et3;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_accuter_screen);

		tb1 = (ToggleButton) findViewById(R.id.toggleButton1);
		tb2 = (ToggleButton) findViewById(R.id.toggleButton11);
		tb3 = (ToggleButton) findViewById(R.id.toggleButton111);

		sb1 = (SeekBar) findViewById(R.id.seekBar1);
		sb2 = (SeekBar) findViewById(R.id.seekBar11);
		sb3 = (SeekBar) findViewById(R.id.seekBar111);

		et1 = (EditText) findViewById(R.id.editText1);
		et2 = (EditText) findViewById(R.id.editText11);
		et3 = (EditText) findViewById(R.id.editText111);

		// if(!tb1.isChecked()){
		// sb1.setEnabled(false);
		// et1.setEnabled(false);
		// }
		// if(!tb2.isChecked()){
		// sb2.setEnabled(false);
		// et2.setEnabled(false);
		// }
		// if(!tb3.isChecked()){
		// sb3.setEnabled(false);
		// et3.setEnabled(false);
		// }
		//
		// tb1.setOnCheckedChangeListener(new OnCheckedChangeListener() {
		//
		// @Override
		// public void onCheckedChanged(CompoundButton buttonView, boolean
		// isChecked) {
		// // TODO Auto-generated method stub
		// if(isChecked){
		// sb1.setEnabled(true);
		// et1.setEnabled(true);
		// }else {
		// sb1.setEnabled(false);
		// et1.setEnabled(false);
		// }
		//
		// }
		// });
		// tb2.setOnCheckedChangeListener(new OnCheckedChangeListener() {
		//
		// @Override
		// public void onCheckedChanged(CompoundButton buttonView, boolean
		// isChecked) {
		// // TODO Auto-generated method stub
		// if(isChecked){
		// sb2.setEnabled(true);
		// et2.setEnabled(true);
		// }else {
		// sb2.setEnabled(false);
		// et2.setEnabled(false);
		// }
		//
		// }
		// });
		// tb3.setOnCheckedChangeListener(new OnCheckedChangeListener() {
		//
		// @Override
		// public void onCheckedChanged(CompoundButton buttonView, boolean
		// isChecked) {
		// // TODO Auto-generated method stub
		// if(isChecked){
		// sb3.setEnabled(true);
		// et3.setEnabled(true);
		// }else {
		// sb3.setEnabled(false);
		// et3.setEnabled(false);
		// }
		//
		// }
		// });

		et1.setText("" + sb1.getProgress());
		et2.setText("" + sb2.getProgress());
		et3.setText("" + sb3.getProgress());

		sb1.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				et1.setText("" + progress);
			}
		});
		sb2.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				et2.setText("" + progress);
			}
		});
		sb3.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				et3.setText("" + progress);
			}
		});

		findViewById(R.id.ll7).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		findViewById(R.id.ll8).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				show();
				
				try {
					JSONObject json = new JSONObject();
					json.put("phone_number", (String) PreferencesManager.getPref(PreferencesManager.keys.NUMBER, ""));

					JSONArray jsonArray = new JSONArray();
					jsonArray.put(tb1.isChecked());
					jsonArray.put(et1.getText().toString());
					json.put("1", jsonArray);

					JSONArray jsonArray1 = new JSONArray();
					jsonArray1.put(tb2.isChecked());
					jsonArray1.put(et2.getText().toString());
					json.put("2", jsonArray1);

					JSONArray jsonArray3 = new JSONArray();
					jsonArray3.put(tb3.isChecked());
					jsonArray3.put(et3.getText().toString());
					json.put("3", jsonArray3);

					AsyncApp42ServiceApi.instance(AccuterScreen.this).insertJSONDoc(IotApplication.DB, IotApplication.TABLE1, json, new App42StorageServiceListener() {

						@Override
						public void onUpdateDocSuccess(Storage response) {
							// TODO Auto-generated method stub

						}

						@Override
						public void onUpdateDocFailed(App42Exception ex) {
							// TODO Auto-generated method stub

						}

						@Override
						public void onInsertionFailed(App42Exception ex) {
							// TODO Auto-generated method stub

						}

						@Override
						public void onFindDocSuccess(Storage response) {
							// TODO Auto-generated method stub

						}

						@Override
						public void onFindDocFailed(App42Exception ex) {
							// TODO Auto-generated method stub

						}

						@Override
						public void onDocumentInserted(Storage response) {
							// TODO Auto-generated method stub
							Toast.makeText(AccuterScreen.this, "Setting Saved", Toast.LENGTH_SHORT).show();
							
							dismiss();
						}
					});

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

		show();
		AsyncApp42ServiceApi.instance(AccuterScreen.this).findDocByKey(IotApplication.DB, IotApplication.TABLE1, "phone_number", (String) PreferencesManager.getPref(PreferencesManager.keys.NUMBER),
				new AsyncApp42ServiceApi.App42StorageServiceListener() {

					@Override
					public void onUpdateDocSuccess(Storage response) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onUpdateDocFailed(App42Exception ex) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onInsertionFailed(App42Exception ex) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onFindDocSuccess(Storage response) {
						// TODO Auto-generated method stub

						try {
							ArrayList<Storage.JSONDocument> jsonDocList = response.getJsonDocList();
							if (jsonDocList.size() > 0) {
								JSONObject jsonDoc = new JSONObject(jsonDocList.get(jsonDocList.size()-1).getJsonDoc());

								JSONArray one = jsonDoc.getJSONArray("1");
								tb1.setChecked(one.getBoolean(0));
								sb1.setProgress(one.getInt(1));

								JSONArray two = jsonDoc.getJSONArray("2");
								tb2.setChecked(two.getBoolean(0));
								sb2.setProgress(two.getInt(1));

								JSONArray three = jsonDoc.getJSONArray("3");
								tb3.setChecked(three.getBoolean(0));
								sb3.setProgress(three.getInt(1));

							}

						} catch (JSONException e) {
							e.printStackTrace();
						}
						
						dismiss();

					}

					@Override
					public void onFindDocFailed(App42Exception ex) {
						// TODO Auto-generated method stub
						dismiss();
					}

					@Override
					public void onDocumentInserted(Storage response) {
						// TODO Auto-generated method stub

					}
				});
	}

	public void cellClicked(View v) {
		switch (v.getId()) {
		case R.id.textView1:

			break;

		default:
			break;
		}
	}
	
	ProgressDialog pd;
	public void show(){
		pd =  new ProgressDialog(AccuterScreen.this);
		pd.setMessage("Processing..");
		pd.setCancelable(false);
		pd.show();
	}
	
	public void dismiss(){
		if(pd != null){
			pd.dismiss();
			pd =  null;
		}
	}

}
