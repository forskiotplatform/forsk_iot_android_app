package in.forsk.iotapp.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.shephertz.app42.paas.sdk.android.App42Exception;
import com.shephertz.app42.paas.sdk.android.storage.Storage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import in.forsk.iotapp.utils.AsyncApp42ServiceApi;
import in.forsk.iotapp.IotApplication;
import in.forsk.iotapp.R;
import in.forsk.iotapp.utils.PreferencesManager;
import in.forsk.iotapp.utils.Utils;

public class Dashboard extends Activity {

    private final static String TAG = Dashboard.class.getSimpleName();

    ImageView img1, img2, img3, img4, img5, img6, img7, img8;
    TextView tv1, tv2, tv3, tv4, tv5, tv6, tv7, tv8;

    public static ArrayList<Storage.JSONDocument> jsonDocList;

    String alert_for = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

//        try {
//            alert_for = (String) PreferencesManager.getPref(PreferencesManager.keys.ALERT_FOR, "");
//
//            Log.d(TAG, "Alert for " + alert_for);
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        img1 = (ImageView) findViewById(R.id.imageView1);
        img2 = (ImageView) findViewById(R.id.imageView2);
        img3 = (ImageView) findViewById(R.id.imageView3);
        img4 = (ImageView) findViewById(R.id.imageView4);
        img5 = (ImageView) findViewById(R.id.imageView5);
        img6 = (ImageView) findViewById(R.id.imageView6);
        img7 = (ImageView) findViewById(R.id.imageView7);
        img8 = (ImageView) findViewById(R.id.imageView8);

        tv1 = (TextView) findViewById(R.id.textView1);
        tv2 = (TextView) findViewById(R.id.textView2);
        tv3 = (TextView) findViewById(R.id.textView3);
        tv4 = (TextView) findViewById(R.id.textView4);
        tv5 = (TextView) findViewById(R.id.textView5);
        tv6 = (TextView) findViewById(R.id.textView6);
        tv7 = (TextView) findViewById(R.id.textView7);
        tv8 = (TextView) findViewById(R.id.textView8);

//        final LinearLayout ll1 = (LinearLayout) findViewById(R.id.ll1);
//        final LinearLayout ll2 = (LinearLayout) findViewById(R.id.ll2);
//        final LinearLayout ll3 = (LinearLayout) findViewById(R.id.ll3);
//        final LinearLayout ll4 = (LinearLayout) findViewById(R.id.ll4);
//        final LinearLayout ll5 = (LinearLayout) findViewById(R.id.ll5);
//        final LinearLayout ll6 = (LinearLayout) findViewById(R.id.ll6);
//        final LinearLayout ll7 = (LinearLayout) findViewById(R.id.ll7);
//        final LinearLayout ll8 = (LinearLayout) findViewById(R.id.ll8);
//        final LinearLayout ll9 = (LinearLayout) findViewById(R.id.ll9);
//        final LinearLayout ll10 = (LinearLayout) findViewById(R.id.ll10);
//
//
//        try {
//            if (alert_for.equalsIgnoreCase("Temp")) {
//
//                ll1.setBackgroundColor(Color.parseColor("#55FF0000"));
//
//            } else if (alert_for.equalsIgnoreCase("POWER")) {
//
//                ll2.setBackgroundColor(Color.parseColor("#55FF0000"));
//            } else if (alert_for.equalsIgnoreCase("HUM")) {
//
//                ll3.setBackgroundColor(Color.parseColor("#55FF0000"));
//            } else if (alert_for.equalsIgnoreCase("LDR")) {
//
//                ll4.setBackgroundColor(Color.parseColor("#55FF0000"));
//            } else if (alert_for.equalsIgnoreCase("PIR")) {
//
//                ll5.setBackgroundColor(Color.parseColor("#55FF0000"));
//            } else if (alert_for.equalsIgnoreCase("DOOR")) {
//
//                ll6.setBackgroundColor(Color.parseColor("#55FF0000"));
//            }
//
//        } catch (Exception e) {
//
//        }

        PreferencesManager.savePref(PreferencesManager.keys.ALERT_FOR, "");

        show("Getting data..");
        AsyncApp42ServiceApi.instance(Dashboard.this).findDocByKey(IotApplication.DB, IotApplication.TABLE, "phone_number", (String) PreferencesManager.getPref(PreferencesManager.keys.NUMBER),
                new AsyncApp42ServiceApi.App42StorageServiceListener() {

                    @Override
                    public void onUpdateDocSuccess(Storage response) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void onUpdateDocFailed(App42Exception ex) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void onInsertionFailed(App42Exception ex) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void onFindDocSuccess(Storage response) {
                        // TODO Auto-generated method stub

                        try {
                            jsonDocList = response.getJsonDocList();

                            Log.d(TAG, jsonDocList.toString());

                            if (jsonDocList.size() > 0) {
                                JSONObject jsonDoc = new JSONObject(jsonDocList.get(jsonDocList.size() - 1).getJsonDoc());

//								tv1.setText(jsonDoc.getString("Temp"));
//								tv2.setText(jsonDoc.getString("POWER"));
//								tv3.setText(jsonDoc.getString("HUM"));
//								tv4.setText(jsonDoc.getString("LDR"));
//								tv5.setText(jsonDoc.getString("PIR"));
//								tv6.setText(jsonDoc.getString("DOOR"));

                                tv1.setText(Utils.getString(jsonDoc, "Temp", "NA"));
                                tv2.setText(Utils.getString(jsonDoc, "POWER", "NA"));
                                tv3.setText(Utils.getString(jsonDoc, "HUM", "NA"));
                                tv4.setText(Utils.getString(jsonDoc, "LDR", "NA"));
                                tv5.setText(Utils.getString(jsonDoc, "PIR", "NA"));
                                tv6.setText(Utils.getString(jsonDoc, "DOOR", "NA"));

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        dismiss();
                    }

                    @Override
                    public void onFindDocFailed(App42Exception ex) {
                        // TODO Auto-generated method stub
                        ex.printStackTrace();
                        ;
                        dismiss();
                    }

                    @Override
                    public void onDocumentInserted(Storage response) {
                        // TODO Auto-generated method stub

                    }
                });

//        new CountDownTimer(5000, 1000) {
//
//            public void onTick(long millisUntilFinished) {
//                //here you can have your logic to set text to edittext
//            }
//
//            public void onFinish() {
//                ll1.setBackgroundColor(Color.parseColor("#FFFFFF"));
//                ll2.setBackgroundColor(Color.parseColor("#FFFFFF"));
//                ll3.setBackgroundColor(Color.parseColor("#FFFFFF"));
//                ll4.setBackgroundColor(Color.parseColor("#FFFFFF"));
//                ll6.setBackgroundColor(Color.parseColor("#FFFFFF"));
//                ll6.setBackgroundColor(Color.parseColor("#FFFFFF"));
//            }
//
//        }.start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        PreferencesManager.savePref(PreferencesManager.keys.ALERT_FOR, "");
    }

    public void cellClicked(View v) {
        Intent intent = new Intent(Dashboard.this, SensorHistory.class);
        Bundle bundle = new Bundle();
        switch (v.getId()) {
            case R.id.ll1:

                bundle.putString("Type", "Temp");
                intent.putExtras(bundle);
                startActivity(intent);
                break;
            case R.id.ll2:
                bundle.putString("Type", "POWER");
                intent.putExtras(bundle);
                startActivity(intent);
                break;
            case R.id.ll3:
                bundle.putString("Type", "HUM");
                intent.putExtras(bundle);
                startActivity(intent);
                break;
            case R.id.ll4:
                bundle.putString("Type", "LDR");
                intent.putExtras(bundle);
                startActivity(intent);
                break;
            case R.id.ll5:
                bundle.putString("Type", "PIR");
                intent.putExtras(bundle);
                startActivity(intent);
                break;
            case R.id.ll6:
                bundle.putString("Type", "DOOR");
                intent.putExtras(bundle);
                startActivity(intent);
                break;
            case R.id.ll7:
//                bundle.putString("Type", "temp");
//                intent.putExtras(bundle);
//                startActivity(intent);
                Toast.makeText(Dashboard.this, "Data now available", Toast.LENGTH_LONG).show();
                break;
            case R.id.ll8:
                Intent intent1 = new Intent(Dashboard.this, AccuterScreen.class);
                startActivity(intent1);
                break;
            case R.id.ll9:
                PreferencesManager.savePref(PreferencesManager.keys.NUMBER, "");
                Intent intent9 = new Intent(Dashboard.this, LoginScreen.class);
                startActivity(intent9);
                finish();
                break;
            case R.id.ll10:
                Intent intent10 = new Intent(Dashboard.this, SettingScreen.class);
                startActivity(intent10);
                break;

            default:
                break;
        }
    }

    ProgressDialog pd;

    public void show(String message) {
        pd = new ProgressDialog(Dashboard.this);
        pd.setMessage(message);
        pd.setCancelable(false);
        pd.show();
    }

    public void dismiss() {
        if (pd != null) {
            pd.dismiss();
            pd = null;
        }
    }
}
