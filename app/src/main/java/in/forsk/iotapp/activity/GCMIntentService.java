package in.forsk.iotapp.activity;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.android.gcm.GCMBaseIntentService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import in.forsk.iotapp.utils.AsyncApp42ServiceApi;
import in.forsk.iotapp.R;
import in.forsk.iotapp.utils.PreferencesManager;

public class GCMIntentService extends GCMBaseIntentService {

    private static final String TAG = "GCMIntentService";

    public GCMIntentService() {
        super("742615932889");
    }

    @Override
    protected void onRegistered(Context context, String registrationId) {
        AsyncApp42ServiceApi.instance(context).getPush()
                .storeDeviceToken((String) in.forsk.iotapp.utils.PreferencesManager.getPref(in.forsk.iotapp.utils.PreferencesManager.keys.NUMBER), registrationId);
    }

    @Override
    protected void onUnregistered(Context context, String registrationId) {
        Log.i(TAG, "Device unregistered");
        // displayMessage(context, getString(R.string.gcm_unregistered));
        // if (GCMRegistrar.isRegisteredOnServer(context)) {
        // ServerUtilities.unregister(context, registrationId);
        // } else {
        // // This callback results from the call to unregister made on
        // // ServerUtilities when the registration to the server failed.
        // Log.i(TAG, "Ignoring unregister callback");
        // }
    }

    @Override
    protected void onMessage(Context context, Intent intent) {
        try {
            Log.i(TAG, "Received message " + intent.getExtras().toString());
            JSONObject jsonObject = new JSONObject(intent.getExtras().getString("message", ""));

            JSONArray alert_for_array = jsonObject.getJSONArray("alert_for");


            JSONArray message_array = jsonObject.getJSONArray("messages");


            for (int i = 0; alert_for_array.length() > i; i++) {

                String alert_for = alert_for_array.getString(i);
                String messages = message_array.getString(i);

                int icon = R.drawable.iot_logo;

                if (alert_for.equalsIgnoreCase("Temp")) {
                    icon = R.drawable.icon_temp;
                } else if (alert_for.equalsIgnoreCase("HUM")) {
                    icon = R.drawable.icon_humidity;
                } else if (alert_for.equalsIgnoreCase("LDR")) {
                    icon = R.drawable.icon_ldr;
                } else if (alert_for.equalsIgnoreCase("POWER")) {
                    icon = R.drawable.icon_power;
                } else if (alert_for.equalsIgnoreCase("PIR")) {
                    icon = R.drawable.icon_pir;
                } else if (alert_for.equalsIgnoreCase("DOOR")) {
                    icon = R.drawable.icon_reed;
                }

                SendNotification(context, "Forsk Lab", messages, icon, alert_for);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDeletedMessages(Context context, int total) {
        Log.i(TAG, "Received deleted messages notification");
        // String message = getString(R.string.gcm_deleted, total);
        // displayMessage(context, message);
        // notifies user
        // generateNotification(context, message);
    }

    @Override
    public void onError(Context context, String errorId) {
        Log.i(TAG, "Received error: " + errorId);
        // displayMessage(context, getString(R.string.gcm_error, errorId));
    }

    @Override
    protected boolean onRecoverableError(Context context, String errorId) {
        return super.onRecoverableError(context, errorId);
    }

    private void SendNotification(Context context, String notificationTitle, String notificationMessage, int icon, String alert_for) {
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        android.app.Notification notification = new android.app.Notification(icon, notificationMessage, System.currentTimeMillis());

        Log.d(TAG, "Alert for " + alert_for);
        Intent notificationIntent = new Intent(this, LoginScreen.class);
        PreferencesManager.savePref(PreferencesManager.keys.ALERT_FOR, alert_for);
        notificationIntent.putExtra("notif_id", "5100");

        // Setting the single top property of the notification and intent.
        notification.defaults = Notification.DEFAULT_ALL;
        notification.flags = Notification.FLAG_AUTO_CANCEL | Notification.FLAG_SHOW_LIGHTS | Notification.FLAG_ONLY_ALERT_ONCE;
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        notification.setLatestEventInfo(context, notificationTitle, notificationMessage, pendingIntent);
        notificationManager.notify(icon, notification);
    }

}
