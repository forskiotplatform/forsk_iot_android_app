package in.forsk.iotapp.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.shephertz.app42.paas.sdk.android.App42Exception;
import com.shephertz.app42.paas.sdk.android.storage.Storage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import in.forsk.iotapp.utils.AsyncApp42ServiceApi;
import in.forsk.iotapp.utils.AsyncApp42ServiceApi.App42StorageServiceListener;
import in.forsk.iotapp.IotApplication;
import in.forsk.iotapp.R;
import in.forsk.iotapp.utils.PreferencesManager;

public class SettingScreen extends Activity implements OnSeekBarChangeListener {

    SeekBar temp_max_sb, temp_min_sb, hum_max_sb, hum_min_sb, ldr_max_sb, ldr_min_sb;
    TextView temp_max_tv, temp_min_tv, hum_max_tv, hum_min_tv, ldr_max_tv, ldr_min_tv;

    ToggleButton powerTb, pirTb, reedTb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_screen);

        temp_max_sb = (SeekBar) findViewById(R.id.temp_max_sb);
        temp_min_sb = (SeekBar) findViewById(R.id.temp_min_sb);

        hum_max_sb = (SeekBar) findViewById(R.id.hum_max_sb);
        hum_min_sb = (SeekBar) findViewById(R.id.hum_min_sb);

        ldr_max_sb = (SeekBar) findViewById(R.id.ldr_max_sb);
        ldr_min_sb = (SeekBar) findViewById(R.id.ldr_min_sb);

        temp_max_tv = (TextView) findViewById(R.id.temp_max_tv);
        temp_min_tv = (TextView) findViewById(R.id.temp_min_tv);

        hum_max_tv = (TextView) findViewById(R.id.hum_max_tv);
        hum_min_tv = (TextView) findViewById(R.id.hum_min_tv);

        ldr_max_tv = (TextView) findViewById(R.id.ldr_max_tv);
        ldr_min_tv = (TextView) findViewById(R.id.ldr_min_tv);

        powerTb = (ToggleButton) findViewById(R.id.powerTb);
        pirTb = (ToggleButton) findViewById(R.id.pirTb);
        reedTb = (ToggleButton) findViewById(R.id.reedTb);

        temp_max_sb.setOnSeekBarChangeListener(this);
        temp_min_sb.setOnSeekBarChangeListener(this);
        hum_max_sb.setOnSeekBarChangeListener(this);
        hum_min_sb.setOnSeekBarChangeListener(this);
        ldr_max_sb.setOnSeekBarChangeListener(this);
        ldr_min_sb.setOnSeekBarChangeListener(this);

        temp_max_tv.setText("" + temp_max_sb.getProgress());
        temp_min_tv.setText("" + temp_min_sb.getProgress());
        hum_max_tv.setText("" + hum_max_sb.getProgress());
        hum_min_tv.setText("" + hum_min_sb.getProgress());
        ldr_max_tv.setText("" + ldr_max_sb.getProgress());
        ldr_min_tv.setText("" + ldr_min_sb.getProgress());

        findViewById(R.id.ll7).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                finish();
            }
        });

        findViewById(R.id.ll8).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                show();

                try {
                    JSONObject json = new JSONObject();
                    json.put("phone_number", (String) PreferencesManager.getPref(PreferencesManager.keys.NUMBER, ""));

                    JSONArray jsonArray = new JSONArray();
                    jsonArray.put(temp_min_sb.getProgress());
                    jsonArray.put(temp_max_sb.getProgress());

                    json.put("Temp", jsonArray);

                    JSONArray jsonArray1 = new JSONArray();
                    jsonArray1.put(powerTb.isChecked() == true ? "ON" : "OFF");
                    json.put("POWER", jsonArray1);

                    JSONArray jsonArray2 = new JSONArray();
                    jsonArray2.put(hum_min_sb.getProgress());
                    jsonArray2.put(hum_max_sb.getProgress());

                    json.put("HUM", jsonArray2);

                    JSONArray jsonArray3 = new JSONArray();
                    jsonArray3.put(ldr_min_sb.getProgress());
                    jsonArray3.put(ldr_max_sb.getProgress());

                    json.put("LDR", jsonArray3);

                    JSONArray jsonArray4 = new JSONArray();
                    jsonArray4.put(pirTb.isChecked() == true ? "YES" : "NO");
                    json.put("PIR", jsonArray4);

                    JSONArray jsonArray5 = new JSONArray();
                    jsonArray5.put(reedTb.isChecked() == true ? "OPEN" : "CLOSE");
                    json.put("DOOR", jsonArray5);

                    AsyncApp42ServiceApi.instance(SettingScreen.this).insertJSONDoc(IotApplication.DB, IotApplication.TABLE2, json, new App42StorageServiceListener() {

                        @Override
                        public void onUpdateDocSuccess(Storage response) {
                            // TODO Auto-generated method stub

                        }

                        @Override
                        public void onUpdateDocFailed(App42Exception ex) {
                            // TODO Auto-generated method stub

                        }

                        @Override
                        public void onInsertionFailed(App42Exception ex) {
                            // TODO Auto-generated method stub

                        }

                        @Override
                        public void onFindDocSuccess(Storage response) {
                            // TODO Auto-generated method stub

                        }

                        @Override
                        public void onFindDocFailed(App42Exception ex) {
                            // TODO Auto-generated method stub

                        }

                        @Override
                        public void onDocumentInserted(Storage response) {
                            // TODO Auto-generated method stub
                            Toast.makeText(SettingScreen.this, "Setting Saved", Toast.LENGTH_SHORT).show();

                            dismiss();
                        }
                    });

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        show();
        AsyncApp42ServiceApi.instance(SettingScreen.this).findDocByKey(IotApplication.DB, IotApplication.TABLE2, "phone_number", (String) PreferencesManager.getPref(PreferencesManager.keys.NUMBER),
                new AsyncApp42ServiceApi.App42StorageServiceListener() {

                    @Override
                    public void onUpdateDocSuccess(Storage response) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void onUpdateDocFailed(App42Exception ex) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void onInsertionFailed(App42Exception ex) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void onFindDocSuccess(Storage response) {
                        // TODO Auto-generated method stub

                        try {
                            ArrayList<Storage.JSONDocument> jsonDocList = response.getJsonDocList();
                            if (jsonDocList.size() > 0) {
                                JSONObject jsonDoc = new JSONObject(jsonDocList.get(jsonDocList.size() - 1).getJsonDoc());


                                JSONArray one = jsonDoc.getJSONArray("Temp");
                                temp_max_sb.setProgress(one.getInt(1));
                                temp_min_sb.setProgress(one.getInt(0));

                                JSONArray two = jsonDoc.getJSONArray("POWER");
                                powerTb.setChecked(two.getString(0).equalsIgnoreCase("ON") ? true : false);

                                JSONArray three = jsonDoc.getJSONArray("HUM");
                                hum_max_sb.setProgress(three.getInt(1));
                                hum_min_sb.setProgress(three.getInt(0));

                                JSONArray four = jsonDoc.getJSONArray("LDR");
                                ldr_max_sb.setProgress(four.getInt(1));
                                ldr_min_sb.setProgress(four.getInt(0));

                                JSONArray five = jsonDoc.getJSONArray("PIR");
                                pirTb.setChecked(five.getString(0).equalsIgnoreCase("YES") ? true : false);

                                JSONArray six = jsonDoc.getJSONArray("DOOR");
                                reedTb.setChecked(six.getString(0).equalsIgnoreCase("OPEN") ? true : false);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        dismiss();

                    }

                    @Override
                    public void onFindDocFailed(App42Exception ex) {
                        // TODO Auto-generated method stub
                        dismiss();
                    }

                    @Override
                    public void onDocumentInserted(Storage response) {
                        // TODO Auto-generated method stub

                    }
                });

    }

    ProgressDialog pd;

    public void show() {
        pd = new ProgressDialog(SettingScreen.this);
        pd.setMessage("Processing..");
        pd.setCancelable(false);
        pd.show();
    }

    public void dismiss() {
        if (pd != null) {
            pd.dismiss();
            pd = null;
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        // TODO Auto-generated method stub
        switch (seekBar.getId()) {
            case R.id.temp_max_sb:
                temp_max_tv.setText("" + progress);
                break;

            case R.id.temp_min_sb:
                temp_min_tv.setText("" + progress);
                break;

            case R.id.hum_max_sb:
                hum_max_tv.setText("" + progress);
                break;

            case R.id.hum_min_sb:
                hum_min_tv.setText("" + progress);
                break;

            case R.id.ldr_max_sb:
                ldr_max_tv.setText("" + progress);
                break;

            case R.id.ldr_min_sb:
                ldr_min_tv.setText("" + progress);
                break;

            default:
                break;
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        // TODO Auto-generated method stub

    }

}
