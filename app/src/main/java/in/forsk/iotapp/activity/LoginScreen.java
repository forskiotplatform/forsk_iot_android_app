package in.forsk.iotapp.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import java.util.HashMap;

import in.forsk.iotapp.GlobalConstant;
import in.forsk.iotapp.R;
import in.forsk.iotapp.utils.CommonUtils;
import in.forsk.iotapp.utils.HttpUtils;
import in.forsk.iotapp.utils.PreferencesManager;

public class LoginScreen extends Activity {

    private final static String TAG = LoginScreen.class.getSimpleName();
    private Context context;

    EditText phoneEt, emailEt;
    Button loginBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this;

        if (!TextUtils.isEmpty(PreferencesManager.getPref(PreferencesManager.keys.NUMBER, ""))) {
            finish();
            Intent intent = new Intent(LoginScreen.this, Dashboard.class);
            startActivity(intent);

            return;
        }

        phoneEt = (EditText) findViewById(R.id.editText1);
        emailEt = (EditText) findViewById(R.id.editText2);
        loginBtn = (Button) findViewById(R.id.button1);

        phoneEt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    loginBtn.performClick();
                    return true;
                }
                return false;
            }
        });


        loginBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                System.out.println("Clicked");

                final String userName = phoneEt.getText().toString().trim();
                final String pass = emailEt.getText().toString().trim();

                HashMap<String, String> params = new HashMap<String, String>();
                params.put("apiKey", GlobalConstant.API_KEY);
                params.put("phone", userName);
                params.put("pass", pass);

                ProgressDialog pd = new ProgressDialog(context);
                pd.setCancelable(false);
                pd.setMessage("Loading...");

                new AQuery(context).progress(pd).ajax(GlobalConstant.API.LOGIN + HttpUtils.getPostDataString(params), JSONObject.class, new AjaxCallback<JSONObject>() {
                    @Override
                    public void callback(String url, JSONObject object, AjaxStatus status) {
                        super.callback(url, object, status);

                        Log.d(TAG,object.toString());

                        if (CommonUtils.getString(object, "error").equalsIgnoreCase("0")) {
                            Intent newIntent = new Intent(LoginScreen.this,Dashboard.class);
                            newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(newIntent);
                        } else {
                            Toast.makeText(LoginScreen.this, CommonUtils.getString(object, "message"), Toast.LENGTH_SHORT).show();

                            Intent newIntent = new Intent(LoginScreen.this,SignupScreen.class);
                            newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(newIntent);
                        }
                    }
                });
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    ProgressDialog pd;

    public void show(String messaage) {
        pd = new ProgressDialog(LoginScreen.this);
        pd.setMessage(messaage);
        pd.setCancelable(false);
        pd.show();
    }

    public void dismiss() {
        if (pd != null) {
            pd.dismiss();
            pd = null;
        }
    }
}
