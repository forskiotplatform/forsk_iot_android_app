package in.forsk.iotapp.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.shephertz.app42.paas.sdk.android.storage.Storage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;

import in.forsk.iotapp.R;
import in.forsk.iotapp.utils.Utils;

public class SensorHistory extends Activity {

    ListView listView;
    ArrayList<Storage.JSONDocument> jsonDocList = new ArrayList<>();

    String sensor_type = "";

    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensor_history);

        if (Dashboard.jsonDocList == null) {
            finish();
            Toast.makeText(SensorHistory.this, "Data now available", Toast.LENGTH_LONG).show();
            return;
        }
        try {
            sensor_type = getIntent().getStringExtra("Type");
        } catch (Exception e) {
            sensor_type = "";
            e.printStackTrace();
        }

        listView = (ListView) findViewById(R.id.listView1);


        jsonDocList.addAll(Dashboard.jsonDocList);

        Collections.reverse(jsonDocList);

        SensorHistoryAdapter adapter = new SensorHistoryAdapter();
        listView.setAdapter(adapter);
    }

    public class SensorHistoryAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return jsonDocList.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = getLayoutInflater().inflate(R.layout.sensor_history_row, null);
            }

            TextView tv = (TextView) convertView.findViewById(R.id.textView2);
            TextView date = (TextView) convertView.findViewById(R.id.textView3);
            ImageView icon = (ImageView) convertView.findViewById(R.id.imageView1);

            try {
                JSONObject jsonDoc = new JSONObject(jsonDocList.get(position).getJsonDoc());

                date.setText(jsonDoc.getString("date"));

                if (sensor_type.equalsIgnoreCase("Temp")) {

                    tv.setText("Temp : " + Utils.getString(jsonDoc, "Temp", "NA"));

                    icon.setImageResource(R.drawable.icon_temp);

                } else if (sensor_type.equalsIgnoreCase("POWER")) {

                    tv.setText("POWER : " + Utils.getString(jsonDoc, "POWER", "NA"));
                    icon.setImageResource(R.drawable.icon_power);
                } else if (sensor_type.equalsIgnoreCase("HUM")) {

                    tv.setText("HUM : " + Utils.getString(jsonDoc, "HUM", "NA"));
                    icon.setImageResource(R.drawable.icon_humidity);
                } else if (sensor_type.equalsIgnoreCase("LDR")) {

                    tv.setText("LDR : " + Utils.getString(jsonDoc, "LDR", "NA"));
                    icon.setImageResource(R.drawable.icon_ldr);
                } else if (sensor_type.equalsIgnoreCase("PIR")) {

                    tv.setText("PIR : " + Utils.getString(jsonDoc, "PIR", "NA"));
                    icon.setImageResource(R.drawable.icon_pir);
                } else if (sensor_type.equalsIgnoreCase("DOOR")) {

                    tv.setText("DOOR : " + Utils.getString(jsonDoc, "DOOR", "NA"));
                    icon.setImageResource(R.drawable.icon_reed);
                }


            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return convertView;
        }
    }

}
