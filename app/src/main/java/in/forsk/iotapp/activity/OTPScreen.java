package in.forsk.iotapp.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import in.forsk.iotapp.GlobalConstant;
import in.forsk.iotapp.R;
import in.forsk.iotapp.utils.HttpUtils;

public class OTPScreen extends AppCompatActivity {

    private final static String TAG = OTPScreen.class.getSimpleName();
    private Context context;

    EditText otpEt;
    Button submitBtn;


    Map<String, String> map;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otpscreen);

        context = this;

        otpEt = (EditText) findViewById(R.id.otpEt);
        submitBtn = (Button) findViewById(R.id.submitBtn);

        Intent intent = getIntent();
        String action = intent.getAction();
        Uri data = intent.getData();

        Toast.makeText(this, data.toString(), Toast.LENGTH_SHORT).show();

        try {
            map = HttpUtils.splitQuery(new URL(data.toString()));

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }


        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String otp = otpEt.getText().toString().trim();


                HashMap<String, String> params = new HashMap<String, String>();
                params.put("apiKey", GlobalConstant.API_KEY);
                params.put("activation_key", map.get("activation_key"));
                params.put("otp", otp);

                ProgressDialog pd = new ProgressDialog(context);
                pd.setCancelable(false);
                pd.setMessage("Loading...");

                new AQuery(context).progress(pd).ajax(GlobalConstant.API.ACCOUNT_ACTIVIATION + HttpUtils.getPostDataString(params), String.class, new AjaxCallback<String>() {
                    @Override
                    public void callback(String url, String object, AjaxStatus status) {
                        super.callback(url, object, status);

                        Toast.makeText(context, object, Toast.LENGTH_LONG).show();
                    }
                });
            }
        });


    }
}
