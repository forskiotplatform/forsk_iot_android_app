package in.forsk.iotapp.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import java.util.HashMap;

import in.forsk.iotapp.GlobalConstant;
import in.forsk.iotapp.R;
import in.forsk.iotapp.utils.HttpUtils;

public class SignupScreen extends AppCompatActivity {
    private final static String TAG = SignupScreen.class.getSimpleName();
    private Context context;

    EditText emailEt, phoneEt, passEt;
    Button signupBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_screen);

        context = this;

        emailEt = (EditText) findViewById(R.id.editText);
        phoneEt = (EditText) findViewById(R.id.editText3);
        passEt = (EditText) findViewById(R.id.editText4);

        signupBtn = (Button) findViewById(R.id.button);


        signupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String email = emailEt.getText().toString().trim();
                String phone = phoneEt.getText().toString().trim();
                String pass = passEt.getText().toString().trim();


                HashMap<String, String> params = new HashMap<String, String>();
                params.put("apiKey", GlobalConstant.API_KEY);
                params.put("email", email);
                params.put("phone", phone);
                params.put("pass", pass);

                ProgressDialog pd = new ProgressDialog(context);
                pd.setCancelable(false);
                pd.setMessage("Loading...");

                new AQuery(context).progress(pd).ajax(GlobalConstant.API.REGISTRATION + HttpUtils.getPostDataString(params), String.class, new AjaxCallback<String>() {
                    @Override
                    public void callback(String url, String object, AjaxStatus status) {
                        super.callback(url, object, status);

                        Toast.makeText(context, object, Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}
