package in.forsk.iotapp;

import android.app.Application;

import com.androidquery.util.AQUtility;

import in.forsk.iotapp.utils.PreferencesManager;

public class IotApplication extends Application {

	public static String DB = "CardPro";
	public static String TABLE = "IOT";
	public static String TABLE1 = "ACT";
	public static String TABLE2 = "RULES";
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		
		PreferencesManager.init(this);

		AQUtility.setDebug(true);
		
	}

}
