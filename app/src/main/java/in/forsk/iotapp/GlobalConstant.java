package in.forsk.iotapp;

/**
 * Created by dev on 4/25/2016.
 */
public class GlobalConstant {
    public static class API{
        public final static String BASE_URL = "http://api-engine.shephertz.com/1.0/";
        public final static String REGISTRATION = BASE_URL+"registration?";
        public final static String SEND_ACTIVIATION = BASE_URL+"send_activiation?";
        public final static String ACCOUNT_ACTIVIATION = BASE_URL+"account_activiation?";
        public final static String LOGIN = BASE_URL+"login?";
    }

    public final static String API_KEY = "a7ef2e069dd6a04a08ee1c3a23364db9c8b624bc738335dfaf7539d4e7d3e14d";
}
