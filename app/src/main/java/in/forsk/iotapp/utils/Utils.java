package in.forsk.iotapp.utils;

import org.json.JSONObject;

/**
 * Created by dev on 4/14/2016.
 */
public class Utils {

    public static String getString(JSONObject jsonObject,String key,String error_string){
        try{
            return jsonObject.getString(key);
        }catch (Exception e){
            return error_string;
        }
    }
}
