package in.forsk.iotapp.utils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by dev on 5/4/2016.
 */
public class CommonUtils {

    public static String getString(JSONObject obj, String key) {
        try {
            return obj.getString(key);
        } catch (JSONException e) {
            return "";
        }
    }

    public static Boolean getBool(JSONObject obj, String key) {
        try {
            return obj.getBoolean(key);
        } catch (JSONException e) {
            return false;
        }
    }
}
